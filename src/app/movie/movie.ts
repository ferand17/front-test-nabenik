export interface Movie{
    movieId: number,
    title: string,
    year: string,
    duration: string,
    actorList?: any[],
}