import { Injectable, PipeTransform } from '@angular/core';
import { Observable} from 'rxjs';
import { Movie } from './movie';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class MovieService {
  //Angular
  constructor(private http: HttpClient) {  }
  //HTTP
  url = 'http://localhost:8080/back-test-2.0-SNAPSHOT/rest/movies';
  gethttpMovies(): Observable<any> {
    return this.http.get(this.url);
  }
  eliminarhttpMovies(id: string): Observable<any> {
    return this.http.delete(this.url+"/"+ id);
  }
  crearhttpMovies(movie: Movie): Observable<any> {
    return this.http.post(this.url, movie);
  }
  UpdatehttpMovies(id: string, movie: Movie): Observable<any> {
    return this.http.put(this.url +"/"+ id, movie);
  }

}