import { Component } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Movie } from './movie';
import { MovieService } from './movie.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css'],
  providers: [MovieService, DecimalPipe]
})


export class MovieComponent {

  //angular
  dataMovies: Movie[] = [];
  constructor(private modalService: NgbModal, public service: MovieService, private loction: Location, private route: Router) {
    this.movieAction = "Insert";
    this.service.gethttpMovies().subscribe(data => {
      data.forEach((element: Movie) => {
        this.dataMovies.push(element)
      });
    })
  }

  //modal
  movieAction: String = "";
  formMovie = new FormGroup({
    movieId: new FormControl(''),
    title: new FormControl(''),
    year: new FormControl(''),
    duration: new FormControl('')
  })
  openInsert(content: any) {
    this.movieAction = "Insert"
    this.modalService.open(content, { centered: true }).result.then(result => { });
  }
  openUpdate(content: any, movie: Movie) {
    this.movieAction = "Update"
    this.formMovie.setValue({
      movieId: movie.movieId,
      title: movie.title,
      year: movie.year,
      duration: movie.duration
    })
    this.modalService.open(content, { centered: true }).result.then(result => { })
  }
  actionOpen() {
    if (this.movieAction === "Insert") {
      let aux: Movie = this.formMovie.value;
      this.service.crearhttpMovies({
        "title": aux.title, "year": aux.year, "duration": aux.duration,
        movieId: 0
      }).subscribe(data => { }, error => {
        console.log(error)
      })
    } else {
      let aux: Movie = this.formMovie.value;
      this.service.UpdatehttpMovies("" + aux.movieId, aux).subscribe()
    }
    this.modalService.dismissAll()
  }
  deleteMovie(id: string) {
    this.service.eliminarhttpMovies(id).subscribe(data => {
    }, error => {
      console.log(error)
    })
  }
}
