import { Component } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public href:String ="";
  title = 'Front-test';
  constructor(location: Location){}
  ngOnInit(){
    if(location.pathname==="/"){
      this.href = "INICIO";
    }else{
      this.href = location.pathname.replace("/","").toUpperCase();
    }
  }
}
