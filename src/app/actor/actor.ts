export interface Actor{
    actorId:number,
    birthday:string,
    country:string,
    name:string,
    movie_id:number
}