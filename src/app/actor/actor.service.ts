import { Actor } from './actor';
import { Injectable} from '@angular/core';
import { Observable} from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class ActorService {
  //Angular
  constructor(private http: HttpClient) {  }
  //HTTP
  url = 'http://localhost:8080/back-test-2.0-SNAPSHOT/rest/actor';
  gethttpActor(): Observable<any> {
    return this.http.get(this.url);
  }
  eliminarhttpActor(id: string): Observable<any> {
    return this.http.delete(this.url+"/"+ id);
  }
  crearhttpActor(actor: Actor): Observable<any> {
    return this.http.post(this.url, actor);
  }
  UpdatehttpActor(id: string, actor: Actor): Observable<any> {
    return this.http.put(this.url +"/"+ id, actor);
  }

}