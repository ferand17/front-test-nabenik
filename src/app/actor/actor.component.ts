import { Component, OnInit } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Actor } from './actor';
import { ActorService } from './actor.service'
import { FormControl, FormGroup } from '@angular/forms';
@Component({
  selector: 'app-actor',
  templateUrl: './actor.component.html',
  styleUrls: ['./actor.component.css']
})
export class ActorComponent{

  dataActor: Actor[] = [];
  constructor(private modalService: NgbModal, public service: ActorService) {
    this.actorAction = "Insert";
    this.service.gethttpActor().subscribe(data => {
      data.forEach((element: Actor) => {
        this.dataActor.push(element)
      });
    })
    console.log(this.dataActor)
  }
  actorAction: String = "";
  formactor = new FormGroup({
    actorId: new FormControl(''),
    birthday: new FormControl(''),
    country: new FormControl(''),
    name: new FormControl(''),
  })
  openInsert(content: any) {
    this.actorAction = "Insert"
    this.modalService.open(content, { centered: true }).result.then(result => { });
  }
  openUpdate(content: any, actor: Actor) {
    this.actorAction = "Update"
    this.formactor.setValue({
      actorId: actor.actorId,
      birthday: actor.birthday,
      country: actor.country,
      name: actor.name,
    })
    
    this.modalService.open(content, { centered: true }).result.then(result => { })
  }
  actionOpen() {
    if (this.actorAction === "Insert") {
      let aux: Actor = this.formactor.value;
      this.service.crearhttpActor({
        "birthday": aux.birthday, "country": aux.country, "name": aux.name, "movie_id": 12, "actorId": 0
      }).subscribe(data => { }, error => {
        console.log(error)
      })
    } else {
      let aux: Actor = this.formactor.value;
      this.service.UpdatehttpActor("" + aux.actorId, aux).subscribe()
    }
    this.modalService.dismissAll()
  }
  deleteActor(id: string) {
    this.service.eliminarhttpActor(id).subscribe(data => {
    }, error => {
      console.log(error)
    })
  }


}
