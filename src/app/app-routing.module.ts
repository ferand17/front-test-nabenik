import { MovieComponent } from './movie/movie.component';
import { InicioComponent } from './inicio/inicio.component';
import { ActorComponent } from './actor/actor.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path:'',component:InicioComponent},
  {path:'actor',component:ActorComponent},
  {path:'movie',component:MovieComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
